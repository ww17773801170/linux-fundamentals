
### 一、Linux Shell中的echo命令

- ### 基本用法

  ###### `echo`命令的基本语法如下：

  ```bash
  echo [选项] [字符串]
  ```

  ###### 其中，`[选项]`是可选的，而`[字符串]`是你想要输出的文本内容。

  ###### 最简单的`echo`命令使用方式是直接跟上想要输出的文本：

  ```bash
  echo Hello, Linux!
  ```

  ###### 执行上述命令后，终端将会显示`Hello, Linux!`。

  ### 转义字符

  ###### 在`echo`命令中，可以使用转义字符`\n`来表示换行。例如：

  ```bash
  echo "First line\nSecond line"
  ```

  ###### 这将会在终端中输出两行文本，第一行是`First line`，第二行是`Second line`。

### 二、什么是环境变量？

- ###### 环境变量是包含关于系统及当前登录用户的环境信息的字符串,一些软件程序使用此信息确定在何处放置文件，环境变量说白了就是指定一个软件的路径

- ###### 环境变量简单来说就是将某些数据，文件或文件夹设置为系统默认值，这样你调用的时候就不用给出完整路径和地址或进行设置，直接用名字就可以了



## 练习

### 创建一个管理员里面建一个普通用户

```
打开ssh,创建一个root管理员
1.apt-get install sudo
2.vim sudoers进入编辑：root  ALL=(ALL:ALL) ALL
                     普通用户： ALL=(ALL:ALL) ALL保存并退出
3.vim /etc/sudoers
4.ls -l /bin/base /bin/sh 改变环境变量
5.新建一个普通用户：useradd ppp，passwd ppp 密码
6.cd /home
7.whereis sudo
8.vim /etc/sudoers
9.在普通用户里： sudo mkdir 中国/广西/百色市/靖西市/壬庄乡/真意村/陆小萍 -p


```

### 创建并编辑日记文件

```
进入姓名文件夹，创建并编辑日记文件：

cd /国家/省份/市/县（区）/镇（街道）/村/姓名
sudo touch 学号姓名.txt
vim 学号姓名进入编辑
sudo chown 主机名 学号姓名
```

### 打包文件夹

```
返回到家目录，使用tar命令打包国家文件夹：

cd ~
tar -czvf 国家.tar.gz 国家
```

### 解压文件

```
tar -xzvf 国家.tar.gz
```