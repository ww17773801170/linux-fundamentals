# 如何利用自己写好的markdown文档，生成网站

准备工作：

1. 有md格式文档，（你们的笔记）
2. 需要一些可以用md文件生成静态html文档的工具，vitepress,或markdownpro



以用vitepress为例：

1. 在D盘用vs code打开

2. 查看打开终端

3. PS D:\> mkdir books //生成 books目录

4. PS D:\> cd ./books  //进入books目录

5. https://vitepress.dev/zh/guide/getting-started  利用vitepress工具，来构建

6. npm add -D vitepress // 利用node来安装vitepress

7. npx vitepress init  // 启用vitepress的设置向导

   1. ```js
      Welcome to VitePress!
      │
      ◇  Where should VitePress initialize the config?
      │  ./docs
      │
      ◇  Site title:
      │  我的笔记我作主
      │
      ◇  Site description:
      │  这是一个用markdown生成的笔记网站
      │
      ◇  Theme:
      │  Default Theme
      │
      ◇  Use TypeScript for config and theme files?
      │  Yes
      │
      ◇  Add VitePress npm scripts to package.json?
      │  Yes
      │
      └  Done! Now run npm run docs:dev and start writing.
      ```

      

8. npm run docs:dev // 本地预览生成的效果

9. 修改index.md可以修改首页的连接

10. 修改config可以修改右上角的导航，和左侧的导航

11. 将笔记的文档复制到docs目录下的对应子目录

12. 利用cmd的tree /F 命令得到所有笔记的文件名，再将

    1. ```js
       items: [
                 { text: 'Markdown Examples', link: '/markdown-examples' },
                 { text: 'Runtime API Examples', link: '/api-examples' }
               ]
       这种要配置的格式扔给AI，让它整合文件名到这种格式
       ```

       

13. 如果你要将笔记放在一个统一的子目录中访问，用在配置文件config中加入一行

     'base':'/books/',

14. npm run docs:build // 将md文件生成html到\docs\.vitepress\dist 目录

15. 登录自己的服务器，使用SSH工具，将dist上传到自己网站的目录

16. mv dist books // 将dist改名为books