### 5-15讲课内容：初识Linux

1. 如何安装一个Debian

 1. 安装一个VM虚拟机

- 1. 下载VM
- 1. 注册VM：百度到处都是序列号

1. 下载Debian的安装镜像ISO

- 1. [https://mirror.lzu.edu.cn/debian-cd/12.5.0/amd64/iso-cd/](https://gitee.com/link?target=https%3A%2F%2Fmirror.lzu.edu.cn%2Fdebian-cd%2F12.5.0%2Famd64%2Fiso-cd%2F)
- 1. [debian-12.5.0-amd64-netinst.iso](https://gitee.com/link?target=https%3A%2F%2Fmirror.lzu.edu.cn%2Fdebian-cd%2F12.5.0%2Famd64%2Fiso-cd%2Fdebian-12.5.0-amd64-netinst.iso)

1. 将Debian安装进VM
2. 创建一个空白虚拟机
3. 配置好相关的硬件参数
4. 关键是修改CD-ROM，选择ISO文件路径
5. 启动虚拟机
6. 在选择语言时，选择简体中文，可以将配置的界面都中文显示
7. 遇到选镜像站点时，选mirror.lzu.edu.cn 兰州大学的站点
8. 软件安装时，只保留最后的标准工具，其它都不选
9. 安装好系统之后，可以用两种帐号登录。

- 1. root 超级管理员，他具有最高的权限。
- 1. 普通用户 dabian2ban 权限很低

1. 默认情况，我们是没有安装SSH服务端的。得自己在debian安装一个SSH的服务端

```
apt-get update // 将软件库更新到最新，得到最新可用的软件列表
apt-get install ssh //安装ssh

// 安装SSH服务端之后，就可以用电脑的SSH客户端进行连接
// cmd,finalshell,tabby，putty
// ssh -l 用户名 ip 
// ssh 用户名@ip
// 但普通用户权限受限，所以需要一些权限时，要么切换到root登录，要么借用root的权限 
root@172.16.90.103's password:
Permission denied, please try again.  // root用户默认是禁止SSH登录的。
// 为了使root可以远程登录，配置SSH服务端
// /etc/ssh/sshd_config // 编辑sshd_config这个文件，但vi编辑器太原始
// 改用vim,
apt-get install vim -y // 安装vim
```

vim /etc/ssh/sshd_config

```
 Port 22 // 开启端口22
 PermitRootLogin yes // 允许root登录
PasswordAuthentication yes // 使用密码验证的模式
PermitEmptyPasswords no // 禁用空密码 
// 重启ssh让配置生效
systemctl restart ssh 
/etc/init.d/ssh  restart


// 默认是命令模式，按i进入编辑模式
// 保存时，按ESC，退回命令模式，按shift+: ，输入qw!
```

linux 执行的服务的命令

1. systemctl 指令 服务名
2. /etc/init.d/服务器 指令
3. 默认也可以用ip addr show 查看ip 地址