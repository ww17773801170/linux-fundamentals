建立一个新用户配置用户
先在root用户建立一个普通用户
useradd -m 用户名 //创建用户
passwd 用户名 //进入用户设置密码
安装sudo配置用户(root)
apt-get install sudo
将新用户加入sudo组内，让用户可以借用权限（root）
usermod -aG usdo user
在新用户更新可用的包
sudo apt-get update
配置用户环境变量（root）
chsh -s /bin/bash 用户名
将新建用户加入环境

创建文件和文件夹，在文件添加内容
创建文件夹
mkdir /文件夹名称  //单个文件
mkdir -p /文件夹名称/。。。。。。 //文件夹下级目录
创建文件
touch 文件名称.文件格式
vim 文件名称 //进入文件编辑模式
压缩文件
安装zip
sudo apt-get install zip
tar压缩文件
tar -cvf 中国.tar 中国
mv 文件名.tar /var/www/yanizwy.top/
mv 文件名.tar /home/user
下载到本地
scp 用户名@服务器ip:目标文件的准确地址  下载的地址
什么是环境变量？
环境变量是操作系统用来存储特定软件环境信息的一种机制。这些变量在系统级别或用户级别设置，用于配置系统行为和程序运行环境。环境变量可以包含诸如路径、配置选项和其他重要信息，供操作系统和应用程序使用

环境变量的作用
配置系统行为：例如，指定临时文件目录的位置。
提供运行时信息：例如，提供当前用户的用户名或主目录路径。
配置软件：例如，设置 Java 的安装路径（JAVA_HOME）。
控制程序的行为：例如，设置调试选项或应用程序模式（生产、开发等）。
常见的环境变量
PATH：定义系统查找可执行文件的目录列表。
HOME：当前用户的主目录路径。
USER：当前用户的用户名。
SHELL：当前用户的默认 shell。
LANG：系统语言和区域设置。
JAVA_HOME：Java 安装目录。

作业用到：
除指定目录及其内的全部子文件，一并强制删除： [root@linuxcool ~]# rm -rf Dir
原文链接：https://www.linuxcool.com/rm

语法格式：cp 参数 源文件名 目标文件名( 复制文件并命名)
原文链接：https://www.linuxcool.com/cp

语法格式： mmove [源文件或目录] [目标文件或目录]
原文链接：https://www.linuxcool.com/mmove

语法格式：cat 参数 文件名
原文链接：https://www.linuxcool.com/cat
在终端设备上显示文件内容

语法格式：rm 参数 文件名 常用参数： -d 仅删除无子文件的空目录   -v 显示执行过程详细信息 -f 强制删除文件而不询问 --help 显示帮助信息 -i 删除文件前询问用户是否确认 --version 显示版本信息 -r 递归删除目录及其内全部子文件
原文链接：https://www.linuxcool.com/rm

语法格式：mv 参数 源文件名 目标文件名(重命名)
原文链接：https://www.linuxcool.com/mv
