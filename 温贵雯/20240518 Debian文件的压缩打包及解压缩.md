# Debian文件的压缩、打包、解压缩

### 明确需求：压缩？打包？解压缩？

## 1、Debian用户模式的设置

```js
//在root模式下输入
adduser username //将 "username" 替换为您想要创建的新用户名
//输入pswd
//给予用户权限（root模式下）
visudo //打开sudoers文件以进行编辑

username   ALL=(ALL:ALL) ALL  //将 "username" 替换为要授予权限的实际用户名。此规则将授予该用户名以sudo权限，允许其以管理员身份运行所有命令。
//下载sudo

apt update//更新软件包列表
apt install sudo//安装 sudo

```

## 创建文件

```js
mkdir Dir1 //创建一个名为Dir1的文件夹
mkdir -p /Dir1/Dir2/Dir3/Dir4/Dir5 //即可创建一个多文件嵌套的文件夹目录
```

## 文件夹的压缩

```js
//tar压缩
tar -cvf archive.tar file1 file2 folder1//打开终端，并使用以下命令将文件/文件夹压缩为.tar文件,将 "archive.tar" 替换为您要创建的.tar文件的文件名。将 "file1"、"file2" 替换为要压缩的具体文件名，将 "folder1" 替换为要压缩的文件夹名。

//如果要压缩整个文件夹及其内容
tar -cvf archive.tar folder//将 "archive.tar" 替换为您要创建的.tar文件的文件名，将 "folder" 替换为要压缩的文件夹名。

//zip压缩
zip -r myfolder.zip myfolder
//-r：递归地打包文件夹中的所有文件和子文件夹。
//myfolder.zip：生成的 ZIP 文件的名称。
//myfolder：要打包的文件夹。
unzip -l myfolder.zip //查看 ZIP 文件的内容
```

报错提示：ZiP压缩

```js
zip -r 中国.zip 中国 提示：zip error: Nothing to do! (try: zip -r 中国.zip . -i 中国)
//使用非了 ASCII 字符（如中文）不要用中文！！！！！！！！！！

zip warning: zip file empty 
文件夹名称错误：指定的文件夹不存在或名称不正确。
文件夹路径错误：指定的路径不正确，导致没有找到文件夹。
文件夹为空：文件夹中没有任何文件或子文件夹。
```

###### debian怎么寻找自己压缩的文件?

```js
//使用 find 命令
find /path/to/search -name "*.zip"
find ~ -name "中国.zip"
//使用 locate 命令
locate "*.zip"

```

#### ZIP错误案例

```js
root@hecs-361560:/# zip -r 中国.zip 中国
updating: 中国/中国.zip (stored 0%)
updating: 中国/安徽省/ (stored 0%)
updating: 中国/安徽省/马鞍山市/ (stored 0%)
updating: 中国/安徽省/马鞍山市/雨山区/ (stored 0%)
updating: 中国/安徽省/马鞍山市/雨山区/钟鼎悦城/ (stored 0%)
updating: 中国/安徽省/马鞍山市/雨山区/钟鼎悦城/陶宗豪/ (stored 0%)
updating: 中国/安徽省/马鞍山市/雨山区/钟鼎悦城/陶宗豪/2244310524陶宗豪.txt (deflated 45%)
adding: 中国/ (stored 0%)
======================================
zip -r 中国.zip 中国 命令实际上是成功的。它显示了压缩过程中的详细信息，包括每个文件和文件夹的更新状态。输出中的 (stored 0%) 和 (deflated 45%) 表示文件和文件夹的压缩比率。
  解释输出
- updating: 中国/中国.zip (stored 0%): 这是一个潜在的问题。你正在将 中国.zip 文件包含在压缩过程中，这可能是因为你在相同的目录下运行了 zip 命令，这是一个常见的陷阱。
- (stored 0%): 表示这些文件夹由于没有实际内容所以没有被压缩。
- (deflated 45%): 表示文件 2244310524陶宗豪.txt 被压缩了45%。


解决方法
为了避免将压缩文件本身包含在压缩过程中，你可以在运行 zip 命令之前将压缩文件名排除在外
zip -r 中国.zip 中国 -x 中国.zip
-x 中国.zip：排除 中国.zip 文件。
目录结构
/root/
└── 中国/
    ├── 安徽省/
    │   └── 马鞍山市/
    │       └── 雨山区/
    │           └── 钟鼎悦城/
    │               └── 陶宗豪/
    │                   └── 2244310524陶宗豪.txt

zip -r 中国.zip 中国 -x 中国.zip
这样可以确保不将新创建的 中国.zip 文件包含在压缩过程中
unzip -l 中国.zip//检查 ZIP 文件的内容
```

## 怎么将压缩后的文件夹导出？Zip、tar都可

```js
使用 HTTP/HTTPS将文件上传到自己的网站上后通过浏览器下载。
将文件复制（移动）到 Web 服务器的目录：
cp /path/to/中国.zip /var/www/html/

zip
通过浏览器访问：
//http://your_server_ip/中国.zip

tar
找到并进入art文件所在的文件夹
sudo mv 中国.tar /var/www/ //借用root权限将tar移动到www下

```

登录自己的域名 [点我试试看](91c13.top/中国.tar)

## zip tar解压缩

```js
tar
找到并进入art文件所在的文件夹
sudo tar -xvf "中国.tar" //借用root权力解压缩

ZIP解压
unzip your_file.zip//将 "your_file.zip" 替换为您要解压缩的.zip文件的实际文件名。


git下载到本地
```js
scp tzh666@110.41.135.85:/中国.tar D:/Desktop

// 普通用户名、@公网IP、tar目录的地址 、本地文件地址 
// “\” 都要用“/”表示

```


## 什么是环境变量？

#### 环境变量是操作系统用来存储特定软件环境信息的一种机制。这些变量在系统级别或用户级别设置，用于配置系统行为和程序运行环境。环境变量可以包含诸如路径、配置选项和其他重要信息，供操作系统和应用程序使用

### 环境变量的作用

- 配置系统行为：例如，指定临时文件目录的位置。
- 提供运行时信息：例如，提供当前用户的用户名或主目录路径。
- 配置软件：例如，设置 Java 的安装路径（JAVA_HOME）。
- 控制程序的行为：例如，设置调试选项或应用程序模式（生产、开发等）。

### 常见的环境变量

- PATH：定义系统查找可执行文件的目录列表。
- HOME：当前用户的主目录路径。
- USER：当前用户的用户名。
- SHELL：当前用户的默认 shell。
- LANG：系统语言和区域设置。
- JAVA_HOME：Java 安装目录。
