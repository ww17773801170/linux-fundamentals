基本用法
echo命令的最基本用法是输出文本：
echo "Hello, World!"
这将在终端上显示Hello, World!。

显示变量
echo命令经常用于显示变量的值。例如：
name="John"
echo "My name is $name"
这将输出My name is John。

输出转义字符
echo命令可以通过使用-e选项来解释转义字符：
echo -e "Hello,\nWorld!"
这将输出：
Hello,
World!

常用的转义字符包括：
\n：换行
\t：制表符（Tab）
\\：反斜杠
\b：退格
禁止换行
默认情况下，echo命令在输出文本后会自动添加一个换行符。如果你不想在输出后添加换行符，可以使用-n选项：
echo -n "Hello, World!"
这将输出Hello, World!，但不会换行。

输出特殊字符
如果你需要在输出中包含特殊字符（如$），你可以使用反斜杠\来转义这些字符：
echo "The price is \$100"
这将输出The price is $100。

结合管道和重定向
echo命令经常与管道（|）和重定向（> 和 >>）操作符一起使用，以将输出传递给其他命令或文件：
echo "Hello, World!" | wc -w  # 将输出传递给wc命令，计算单词数
echo "This is a test" > test.txt  # 将文本重定向到文件test.txt
echo "Another line" >> test.txt  # 将文本追加到文件test.txt


环境变量
环境变量说白了就是指定一个软件的路径，方法如下：
点击我的电脑然后按右键——属性——高级——环境变量——新建——名称/路径

