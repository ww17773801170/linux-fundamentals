初识Linux

1. Linux发展史

2. Linux与Windows区别

3. 为什么我们选择Debian为教学的发行版本

4. 如何安装一个Debian

   1. 安装一个VM虚拟机

      1. 下载VM
      2. 注册VM：百度到处都是序列号

   2. 下载Debian的安装镜像ISO

      1. https://mirror.lzu.edu.cn/debian-cd/12.5.0/amd64/iso-cd/
      2. [debian-12.5.0-amd64-netinst.iso](https://mirror.lzu.edu.cn/debian-cd/12.5.0/amd64/iso-cd/debian-12.5.0-amd64-netinst.iso)

   3. 将Debian安装进VM

      1. 创建一个空白虚拟机
      2. 配置好相关的硬件参数
      3. 关键是修改CD-ROM，选择ISO文件路径
      4. 启动虚拟机
      5. 在选择语言时，选择简体中文，可以将配置的界面都中文显示
      6. 遇到选镜像站点时，选mirror.lzu.edu.cn 兰州大学的站点
      7. 软件安装时，只保留最后的标准工具，其它都不选

   4. 安装好系统之后，可以用两种帐号登录。

      1. root 超级管理员，他具有最高的权限。 
      2. 普通用户 dabian2ban 权限很低

   5. 默认情况，我们是没有安装SSH服务端的。得自己在debian安装一个SSH的服务端

      ```js
      apt-get update // 将软件库更新到最新，得到最新可用的软件列表
      apt-get install ssh //安装ssh
      
      // 安装SSH服务端之后，就可以用电脑的SSH客户端进行连接
      // cmd,finalshell,tabby，putty
      // ssh -l 用户名 ip 
      // ssh 用户名@ip
      // 但普通用户权限受限，所以需要一些权限时，要么切换到root登录，要么借用root的权限 
      root@172.16.90.103's password:
      Permission denied, please try again.  // root用户默认是禁止SSH登录的。
      // 为了使root可以远程登录，配置SSH服务端
      // /etc/ssh/sshd_config // 编辑sshd_config这个文件，但vi编辑器太原始
      // 改用vim,
      apt-get install vim -y // 安装vim
      ```

      vim /etc/ssh/sshd_config 

      ```js
       Port 22 // 开启端口22
       PermitRootLogin yes // 允许root登录
      PasswordAuthentication yes // 使用密码验证的模式
      PermitEmptyPasswords no // 禁用空密码 
      // 重启ssh让配置生效
      systemctl restart ssh 
      /etc/init.d/ssh  restart
      
      
      // 默认是命令模式，按i进入编辑模式
      // 保存时，按ESC，退回命令模式，按shift+: ，输入qw!
      ```

      linux 执行的服务的命令

      1. systemctl 指令 服务名
      2. /etc/init.d/服务器 指令

      

   6. 默认也可以用ip addr show 查看ip 地址

1.  ###  学习vim

1.进入插入模式的三种方法

​       1.i切换进入插入模式后是从光标当前位置开始输入文件

 ```js
#include<stdio.h>
    int main(){ 
       printf("hello world!");
        return 0;
    }
 ```

​      2.按a是从目前光标所在位置的下一个位置开始输入文字

​      3.按0是插入新的一行，从首行开始输入文字

2.移动光标

```js
简单的移动光标
0 → 数字零，到行头
^ → 到本行第一个不是blank字符的位置（所谓blank字符就是空格，tab，换行，回车等）
$ → 到本行行尾
g_ → 到本行最后一个不是blank字符的位置。
/pattern → 搜索 pattern 的字符串（陈皓注：如果搜索出多个匹配，可按n键到下一个）
```

1. 拷贝/粘贴

   （陈皓注：p/P都可以，p是表示在当前位置之后，P表示在当前位置之前）

   > - `P` → 粘贴
   > - `yy` → 拷贝当前行当行于 `ddP`

2. Undo/Redo

   > - `u` → undo
   > - `<C-r>` → redo

3. 打开/保存/退出/改变文件

   (Buffer)

   > - `:e <path/to/file>` → 打开一个文件
   > - `:w` → 存盘
   > - `:saveas <path/to/file>` → 另存为 `<path/to/file>`
   > - `:x`， `ZZ` 或 `:wq` → 保存并退出 (`:x` 表示仅在需要时保存，ZZ不需要输入冒号并回车)
   > - `:q!` → 退出不保存 `:qa!` 强行退出所有的正在编辑的文件，就算别的文件有更改。
   > - `:bn` 和 `:bp` → 你可以同时打开很多文件，使用这两个命令来切换下一个或上一个文件。（陈皓注：我喜欢使用:n到下一个文件）