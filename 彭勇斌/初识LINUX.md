## 初识LINUX

一种操作系统内核，有centos，debian，redhat等。创建于1991年的基于UNIX的开源操作系统。Linux操作系统还带有图形用户界面（GUI），其中包含一些日常使用的必要软件。它也用于运行Linux的台式计算机，移动设备，游戏机，数字存储设备，电子书阅读器，照相机，录像机中。

### SSH登录

#### 查看ip地址

```
ip addr show
```

#### 更新数据

```
apt-get update 
apt-get install net-tools
ifconfig
```

### ssh：

```
apt-get update  更新软件库
apt-get install ssh  安装ssh
```

安装ssh服务端后，可以用电脑的ssh客户端连接

```
ssh -l 用户名 ip
ssh root@ip
```

```
apt-get install vim  安装vim
vim /etc/ssh/sshd_config
```

按 i 进入编辑模式

```
port 22 //开启端口222，将前面的#去掉
PermitRootLogin yes // 允许root登录，将前面的#去掉
PasswordAuthentication yes // 使用密码验证的模式，将前面的#去掉
PermitEmptyPasswords no // 禁用空密码 ，将前面的#去掉
```

按esc退出编辑模式，按住shift: ，再输入wq!，强制保存

重启ssh

```
systemctl restart ssh
/etc/init.d/ssh restart
systemctl status ssh  //查看状态
```

### sudo:

```
su root 更换给root模式
apt-get install sudo
sudo vim /etc/sudoers
root all=(all:all) all
用户名 all=(all:all) all 添加用户名
```