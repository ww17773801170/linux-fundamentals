## 建立一个新用户配置用户
### 先在root用户建立一个普通用户
```
useradd -m 用户名 //创建用户
passwd 用户名 //进入用户设置密码
```
#### 删除用户
```
userdel 用户名
```
### 安装sudo配置用户(root)
```
apt-get install sudo
```
#### 将新用户加入sudo组内，让用户可以借用权限（root）
```
usermod -aG sudo landawang

```
#### 在新用户更新可用的包
```
sudo apt-get update
```
### 配置用户环境变量（root）
```
chsh -s /bin/bash 用户名
```
将新建用户加入环境
## 创建文件和文件夹，在文件添加内容
### 创建文件夹
```
mkdir /文件夹名称  //单个文件
mkdir -p /文件夹名称/。。。。。。 //文件夹下级目录

```
#### 删除文件夹
```
rm -r 要删除的文件夹名称
```
### 创建文件
```
touch 文件名称.文件格式
vim 文件名称 //进入文件编辑模式
``` 
### 压缩文件
#### 安装zip
```
sudo apt-get install zip
```
#### 用zip压缩文件
```
zip archive.zip file1 //单个文件打包 file1（文件名）
zip archive.zip file1 file2 file3  // 多个文件打包

zip -r archive.zip directory //压缩整个目录及其子目录 archive.zip(打包后的名字)  directory(要打包的文件夹)
zip -r archive.zip directory -x "file_or_directory" //打包时排除某些文件或目录
```

#### 解压
##### 安装unzip
```
apt-get install unzip
```
##### 用unzip解压
```
unzip file.zip
```
## 下载到本地
```
scr 用户名@服务器ip:目标文件的准确地址  下载的地址
                   （用/）              (用/)
```