##  Linux

### 一、**什么是linux？**

Linux是创建于1991年的基于UNIX的开源操作系统。Linux操作系统还带有图形用户界面（GUI），其中包含一些日常使用的必要软件。它也用于运行Linux的台式计算机，移动设备，游戏机，数字存储设备，电子书阅读器，照相机，录像机中。

### 二、**Linux与Windows之间的主要区别**

①Linux是开源操作系统，而Windows OS是商业操作系统。

②Linux有权访问源代码并根据用户需要更改代码，而Windows则无权访问源代码。即使在现代桌面环境和操作系统功能下，Linux的运行速度也比Windows最新版本快，而在较旧的硬件上，Windows运行速度较慢。

③Linux发行版不收集用户数据，而Windows收集所有引起隐私保护的用户详细信息。

### 三、如何安装一个Debian

#### 1、安装一个VM虚拟机

#### 2、下载Debian的安装镜像ISO

#### 3、将Debian安装进VM

```
1、创建一个空白虚拟机

2、配置好相关的硬件参数

3、在CD-ROM中，选择ISO文件路径

4、启动虚拟机

5、选择install

5、语言选择中文，可以将配置的界面中文显示

6、分区方法选择整个

7、分区方案选择将所有文件放在同一个分区中

8、将改动写入磁盘，选择是

9、扫描额外的介质，选择否

10、镜像站点所在的国家选择中国

11、Debian仓库镜像站点选择mirror.lzu.edu.cn兰州大学的站点

12、不参加软件包流行调查

13、软件安装时，保留最后的标准工具，其他的都不选
```

#### 4、安装好系统之后，用root超级管理员登录，具有最高权限，也可以普通用户登录，权限低

#### 5、需要自己在Debian中安装一个ssh的服务端，因为默认情况下，我们没有安装ssh服务端

```
apt-get update //更新软件库

apt-get install ssh //安装ssh
```

##### 安装ssh服务端后，可以用电脑的ssh客户端连接

```
ssh -l 用户名 ip

ssh root@ip

ip address show //查看IP地址
```

##### 普通用户权限受限，需要切换到root登录，或者借用root的权限

```
Permission denied, please try again.  // root用户默认是禁止SSH登录的。
```

##### 为了使root可以远程登陆，我们需要去配置ssh服务端

##### /etc/ssh/sshd_config //编辑ssh_config这个文件，vi编辑器原始，可以改用vim，但是里面没有，我们需要自己下载vim

```
apt-get install vim -y //安装vim
```

##### 进入中国ssh_config后，我们可以对里面的一些配置信息做一些改动，按 i 进入编辑模式

```
port 22 //开启端口222，将前面的#去掉

PermitRootLogin yes // 允许root登录，将前面的#去掉

PasswordAuthentication yes // 使用密码验证的模式，将前面的#去掉

PermitEmptyPasswords no // 禁用空密码 ，将前面的#去掉
```

##### 按esc退出编辑模式，按住shift: ，再输入wq!，强制保存

##### 最后，我们需要重启ssh，使配置生效

```
systemctl restart ssh

/etc/init.d/ssh restart

systemctl status ssh //查看状态
```

##### linux 执行的服务的命令

1. systemctl 指令 服务名
2. /etc/init.d/服务器 指令