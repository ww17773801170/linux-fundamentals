初识Linux

1. Linux发展史

2. Linux与Windows区别

3. 为什么我们选择Debian为教学的发行版本

4. 如何安装一个Debian

   1. 安装一个VM虚拟机

      1. 下载VM
      2. 注册VM：百度到处都是序列号

   2. 下载Debian的安装镜像ISO

      1. https://mirror.lzu.edu.cn/debian-cd/12.5.0/amd64/iso-cd/
      2. [debian-12.5.0-amd64-netinst.iso](https://mirror.lzu.edu.cn/debian-cd/12.5.0/amd64/iso-cd/debian-12.5.0-amd64-netinst.iso)

   3. 将Debian安装进VM

      1. 创建一个空白虚拟机
      2. 配置好相关的硬件参数
      3. 关键是修改CD-ROM，选择ISO文件路径
      4. 启动虚拟机
      5. 在选择语言时，选择简体中文，可以将配置的界面都中文显示
      6. 遇到选镜像站点时，选mirror.lzu.edu.cn 兰州大学的站点
      7. 软件安装时，只保留最后的标准工具，其它都不选

   4. 安装好系统之后，可以用两种帐号登录。

      1. root 超级管理员，他具有最高的权限。 
      2. 普通用户 dabian2ban 权限很低

   5. 默认情况，我们是没有安装SSH服务端的。得自己在debian安装一个SSH的服务端

      ```js
      apt-get update // 将软件库更新到最新，得到最新可用的软件列表
      apt-get install ssh //安装ssh
      
      // 安装SSH服务端之后，就可以用电脑的SSH客户端进行连接
      // cmd,finalshell,tabby，putty
      // ssh -l 用户名 ip 
      // ssh 用户名@ip
      // 但普通用户权限受限，所以需要一些权限时，要么切换到root登录，要么借用root的权限 
      root@172.16.90.103's password:
      Permission denied, please try again.  // root用户默认是禁止SSH登录的。
      // 为了使root可以远程登录，配置SSH服务端
      // /etc/ssh/sshd_config // 编辑sshd_config这个文件，但vi编辑器太原始
      // 改用vim,
      apt-get install vim -y // 安装vim
      ```

      vim /etc/ssh/sshd_config 

      ```js
       Port 22 // 开启端口22
       PermitRootLogin yes // 允许root登录
      PasswordAuthentication yes // 使用密码验证的模式
      PermitEmptyPasswords no // 禁用空密码 
      // 重启ssh让配置生效
      systemctl restart ssh 
      /etc/init.d/ssh  restart
      
      
      // 默认是命令模式，按i进入编辑模式
      // 保存时，按ESC，退回命令模式，按shift+: ，输入qw!
      ```

      linux 执行的服务的命令

      1. systemctl 指令 服务名
      2. /etc/init.d/服务器 指令

      

   6. 默认也可以用ip addr show 查看ip 地址

   7. 小练习，

      1. 自行让你的Deiban可用ifconfig命令，这个指令可以用来查看ip 
      2. 学习VIM常用指令