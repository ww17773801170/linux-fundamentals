|      |                                                              |                                             |
| ---- | ------------------------------------------------------------ | ------------------------------------------- |
|      | [6](https://gitee.com/implementation-level-22/linux-fundamentals/commit/a5a65d2d3093980311ddcb0d8d68371438f26f88#aa80175a25b3d7c6f268658b77c715da2f7dcc0f_0_6) | 1. 国家                                     |
|      | [7](https://gitee.com/implementation-level-22/linux-fundamentals/commit/a5a65d2d3093980311ddcb0d8d68371438f26f88#aa80175a25b3d7c6f268658b77c715da2f7dcc0f_0_7) | 1. 省份                                     |
|      | [8](https://gitee.com/implementation-level-22/linux-fundamentals/commit/a5a65d2d3093980311ddcb0d8d68371438f26f88#aa80175a25b3d7c6f268658b77c715da2f7dcc0f_0_8) | 1. 市                                       |
|      | [9](https://gitee.com/implementation-level-22/linux-fundamentals/commit/a5a65d2d3093980311ddcb0d8d68371438f26f88#aa80175a25b3d7c6f268658b77c715da2f7dcc0f_0_9) | 1. 县（区）                                 |
|      | [10](https://gitee.com/implementation-level-22/linux-fundamentals/commit/a5a65d2d3093980311ddcb0d8d68371438f26f88#aa80175a25b3d7c6f268658b77c715da2f7dcc0f_0_10) | 1. 镇（街道）                               |
|      | [11](https://gitee.com/implementation-level-22/linux-fundamentals/commit/a5a65d2d3093980311ddcb0d8d68371438f26f88#aa80175a25b3d7c6f268658b77c715da2f7dcc0f_0_11) | 1. 村（小区）                               |
|      | [12](https://gitee.com/implementation-level-22/linux-fundamentals/commit/a5a65d2d3093980311ddcb0d8d68371438f26f88#aa80175a25b3d7c6f268658b77c715da2f7dcc0f_0_12) | 1. 姓名                                     |
|      | [13](https://gitee.com/implementation-level-22/linux-fundamentals/commit/a5a65d2d3093980311ddcb0d8d68371438f26f88#aa80175a25b3d7c6f268658b77c715da2f7dcc0f_0_13) | 2. 在姓名文件夹，建立一个自己的学号姓名文件 |
|      | [14](https://gitee.com/implementation-level-22/linux-fundamentals/commit/a5a65d2d3093980311ddcb0d8d68371438f26f88#aa80175a25b3d7c6f268658b77c715da2f7dcc0f_0_14) | 3. 在这个文件中，写内容                     |
|      | [15](https://gitee.com/implementation-level-22/linux-fundamentals/commit/a5a65d2d3093980311ddcb0d8d68371438f26f88#aa80175a25b3d7c6f268658b77c715da2f7dcc0f_0_15) | 4. 最后，将国家这个文件夹打包，解压缩       |
|      | [16](https://gitee.com/implementation-level-22/linux-fundamentals/commit/a5a65d2d3093980311ddcb0d8d68371438f26f88#aa80175a25b3d7c6f268658b77c715da2f7dcc0f_0_16) | 5. 下载到本地                               |

#   使用命令，在自己的家目录，以以下层次依次建立文件夹



1  进入家目录  cd /home

2  创建     mkdir  中国/河南省..........  -p

3  可以测试一下  ls (看有无东西)

4  要是在文件夹里面创建txt文本 要用touch 方法      touch  中国.txt   或者  vi   文件.txt

5  tar   -cf  压缩包名称.tar  要压缩的文件/路径      

6  tar  - xvf   压缩包名称.tar        解压

#  环境变量

~~~js
环境变量是包含诸如驱动器、路径或文件名之类的字符串。环境变量控制着多种程序的行为。

任何用户都可以添加、修改或删除用户的环境变量。但是，只有管理员才能添加、修改或删除系统环境变量。

环境变量简单来说就是将某些数据，文件或文件夹设置为系统默认值，这样调用的时候就不用给出完整路径和地址或进行设置，直接用名字就可以了

环境变量是包含关于系统及当前登录用户的环境信息的字符串,一些软件程序使用此信息确定在何处放置文件(如临时文件).
~~~

