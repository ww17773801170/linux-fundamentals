# 课堂笔记

### Debian Linux目录结构简介

Debian Linux采用了典型的Linux文件系统结构，以下是其主要目录结构：

/  目录是Linux文件系统的根目录，包含了整个文件系统的基础结构



/bin  目录是"binary"的缩写，存放着系统启动时需要的最基本的命令。这些命令通常是所有用户都需要的，因此放在这个目录下。它包含一些最常用的命令，如ls、cp、mv等，用于列出目录内容、复制文件、移动文件等常见操作



/boot  目录包含了引导加载程序以及内核文件。在系统启动时，计算机首先加载引导加载程序，然后引导加载程序再加载内核文件，从而启动操作系统



/dev  目录包含了设备文件，这些文件用于与系统中的硬件设备进行通信。在Linux中，一切皆文件，因此硬件设备也被抽象为文件的形式存在于/dev目录中



/etc  目录存放系统的配置文件。这些配置文件包括各种应用程序、服务以及系统本身的配置信息，如网络配置、软件包管理器配置等



/home 用户的家目录，每个用户都有一个以其用户名命名的目录。每个用户都有一个独立的家目录，用于存放其个人文件和设置



/lib  目录存放着系统所需的共享库文件，这些库文件为系统运行时所必须的。在系统启动过程中，引导加载程序会加载这些共享库，以便其他程序能够正常运行



/media  目录是用于挂载可移动介质的挂载点。当插入可移动介质（如USB闪存驱动器、光盘等）时，系统会将其挂载到此目录下，用户可以通过该目录访问其内容



/mnt  目录是用于手动挂载临时文件系统的挂载点。通常情况下，可移动介质等临时挂载的文件系统会被挂载到/media  目录下，而/mnt  目录则用于手动挂载其他临时文件系统，如远程文件系统等



/opt  目录是可选的第三方软件包的安装目录。一些软件包可能选择将其安装到/opt  目录下，而不是/usr  目录下，以便更好地管理



/proc  目录是一个虚拟文件系统，它提供了有关系统和运行进程的信息。在/proc  目录下，每个运行的进程都有一个对应的子目录，其中包含了有关该进程的详细信息



/root  目录是root用户的家目录。与普通用户的家目录类似，root用户也有一个独立的家目录，用于存放其个人文件和设置



/run  目录是一个运行临时文件目录，用于存放系统启动过程中产生的临时文件以及运行时需要的其他临时文件



/sbin  马路存放着系统管理员需要的系统管理命令。与/bin  目录类似，单/sbin  马路中的命令通常只能由系统管理员或具有管理员权限的用户执行



/srv  目录存放着服务的数据目录。一些服务可能会将其数据文件存放在/srv  目录下，以便更好地管理和组织



/tmp 目录是一个临时文件目录，用于存放临时文件。该目录中的文件通常在系统重启后会被删除，因此适合存放一些临时性的数据



/usr  目录包含了大部分用户安装的程序和文件。该目录下包含了各种应用程序、库文件、帮助文档等，是系统中最常用的目录之一