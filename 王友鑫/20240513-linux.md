1、新建文件夹note

2、右键通过vscode打开

3、PS D:\> mkdir books //生成 books目录

4、PS D:\> cd ./books //进入books目录

5、https://vitepress.dev/zh/guide/getting-started 利用vitepress工具，来构建

7、npm add -D vitepress // 利用node来安装vitepress

8、npx vitepress init // 启用vitepress的设置向导
```js
1. Welcome to VitePress!
   │
   ◇  Where should VitePress initialize the config?
   │  ./docs
   │
   ◇  Site title:
   │  我的笔记我作主
   │
   ◇  Site description:
   │  这是一个用markdown生成的笔记网站
   │
   ◇  Theme:
   │  Default Theme
   │
   ◇  Use TypeScript for config and theme files?
   │  Yes
   │
   ◇  Add VitePress npm scripts to package.json?
   │  Yes
   │
   └  Done! Now run npm run docs:dev and start writing.
```
9、npm run docs:dev // 本地预览生成的效果

   1. 修改index.md可以修改首页的连接

   2. 修改config可以修改右上角的导航，和左侧的导航

   3. 将笔记的文档复制到docs目录下的对应子目录
   
      1、在docs文件夹下新建你需要放笔记的文件（把笔记丢进去）
   
      如：![alt text](./imgs/docs下文件.png)
   
10、利用cmd的tree /F 命令得到所有笔记的文件名，再将

   ![alt text](./imgs/cmd图片.png)

   1. items: [
      
              { text: 'Markdown Examples', link: '/markdown-examples' },
              { text: 'Runtime API Examples', link: '/api-examples' }
            ]

   ![alt text](./imgs/AI生成.png)
      
      这种要配置的格式扔给AI，让它整合文件名到这种格式

11、如果你要将笔记放在一个统一的子目录中访问，用在配置文件config中加入一行 'base':'/books/',

12、npm run docs:build // 将md文件生成html到\docs\.vitepress\dist 目录

13、登录自己的服务器，使用SSH工具，将dist上传到自己网站的目录
![alt text](./imgs/ssh工具.png)

14、mv dist books // 将dist改名为books
![alt text](./imgs/改名.png)