## 文件目录指令
### pwd指令

基本语法：

pwd

用于显示当前目录的路径
### ls指令

基本语法:
```
ls [选项] [目录或是文件]
常用选项
-a ：显示当前目录所有的文件和目录，包括隐藏的。
-l ：以列表的方式显示信息，相当于ll
```
### cd指令

基本语法：

cd [参数]
功能描述：切换到指定目录

常用参数
绝对路径(以/开头的目录)和相对路径(以目录名开头的目录，从当前目录下开始查找)

cd ~ 或者cd ：回到自己的主目录

cd .. 回到当前目录的上一级目录
### mkdir命令

基本语法：

mkdir [选项] 要创建的目录
常用选项
-p ：创建多级目录
### rmdir指令

基本语法：

rmdir 目录

功能描述：删除一个空目录
### touch指令

基本语法：

touch 文件名称列表

功能描述：创建一个或多个空文件
### cp指令

基本语法：

cp [选项] source dest
常用选项：
-r ：递归复制整个文件夹
### rm指令

基本语法：

rm [选项] 要删除的文件或目录
常用选项：
-r ：递归删除整个文件夹

-f ：强制删除不提示
### mv指令

基本语法：

mv oldNameFile newNameFile (功能描述：重命名)
mv /temp/movefile /targetFolder (功能描述：移动文件)
### cat指令

基本语法:

cat [选项] 要查看的文件名
常用选项
-n ：显示行号
### more指令

基本语法：

more 要查看的文件名

功能说明：more指令是一个基于VI编辑器的文本过滤器，它以全屏幕的方式按页显示文本文件的内容。
### less指令

基本语法：

less 要查看的文件名

功能说明：less指令用来分屏查看文件内容，它的功能与more指令类似，但是比more指令更加强大，支持各种显示终端。less指令在显示文件内容时，并不是一次将整个文件加载之后才显示，而是根据显示需要加载内容，对于显示大型文件具有较高的效率。
### head指令

基本语法：

head 文件(功能描述：默认查看文件头10行内容)
head -n 5 文件(功能描述：查看文件头5行内容，5可以是任意行数)
### tail指令

基本语法：

tail 文件（功能描述：默认查看文件尾10行内容）
tail -n 5 文件（功能描述：查看文件尾5行内容，5可以是任意行数）
### echo指令

基本语法：

echo [选项] [输出内容]

功能介绍：输出变量或常量内容到控制台